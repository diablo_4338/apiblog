###Описание

Для авторизации(получения токена) необходимо сделать следующий запрос:
```curl -F 'password=test1test' -F 'username=admin' -X POST '<host:port>/auth/'```
В ответе вернется токен, который нужно добавить в заголовок для последующих запросов к сервису
``'Authorization: Token <token>``
Запросы необходимо слать с заголовком **Content-type: application/x-www-form-urlencoded**, иначече запросы PUT,POST могут работать некорректно. Добавить контент можно авторизовавшись в админке(/admin), предустановлены следующие пользователи:
####admin, admin1, admin2, пароль test1test
###Запуск сервиса
```docker-compose up runserver```
###Примеры запросов и описание
#####Список категорий:
```curl '<host:port>/categories/' -H 'Authorization: Token <token>'```

#####Посты в категории:
```curl '<host:port>/categories/<category_id>/' -H 'Authorization: Token <token>'```

#####Один пост:
```curl '<host:port>/posts/<post_id>/' -H 'Authorization: Token <token>'```

#####Редактирование поста:
```curl -F 'text=Foo' -F 'title=Bar' -F 'category=1' -F 'picture=' -X PUT '<host:port>/posts/<post_id>/' -H 'Authorization: Token <token>'```
Доступные для редактирования поля: title, text, category, picture. Обновляются все поля, которые пришли в запросе. Прислать можно только то, которое необходимо обновить !!! Чтобы удалить привязанную картинку, нужно прислать пустое поле ***picture***(как в примере)
#####Добавление поста:
```curl -F 'text=Foo' -F 'title=Bar' -F 'category=1' -F 'picture=@image.jpg' -X POST '<host:port>/posts/' -H 'Authorization: Token <token>'```
Необходимо отправить 3 обязательных поля: title, text, category. Поля id, author - игнорируются и генерируются автоматически
#####Удаление поста:
```curl -X DELETE '<host:port>/posts/<post_id>/' -H 'Authorization: Token <token>'```

#####Добавить в избранное:
```curl -F 'post=81' -F 'action=mark' -X POST '<host:port>/favorite/' -H 'Authorization: Token <token>'```

#####Получить Избранное:
```curl '<host:port>/favorite/' -H 'Authorization: Token <token>'```

#####Удалить из избранного:
```curl -F 'post=81' -F 'action=unmark' -X POST '<host:port>/favorite/' -H 'Authorization: Token <token>'```

#####Авторизация:
```curl -F 'password=test1test' -F 'username=admin' -X POST '<host:port>/auth/'```