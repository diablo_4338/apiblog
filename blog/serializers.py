from sys import getsizeof
from rest_framework import serializers
from .models import Category, Post
from django.utils.six import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import Image


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'title']


class PostSerializer(serializers.ModelSerializer):

    def validate_picture(self, picture):
        if not picture:
            return None
        img = Image.open(picture)
        if img.format not in ['JPEG', 'PNG']:
            # TODO raise
            pass
        width, height = img.size
        exif = None
        if 'exif' in img.info:
            exif = img.info['exif']
        if width > 1024 or height > 768:
            img.thumbnail((1024, 768), Image.ANTIALIAS)
        elif width > 768 or height > 1024:
            img.thumbnail((768, 1024), Image.ANTIALIAS)
        output = BytesIO()
        if exif:
            img.save(output, format=img.format, exif=exif)
        else:
            img.save(output, format=img.format)
        output.seek(0)
        return InMemoryUploadedFile(output, 'ImageField', picture.name,
                                    'image/%s' % img.format.lower(),
                                    getsizeof(output), None)

    class Meta:
        model = Post
        fields = ["id",
                  "title",
                  "text",
                  "picture",
                  "author",
                  "category",
                  ]


class FavoriteSerializer(serializers.Serializer):
    action = serializers.CharField()
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    def validate_action(self, action):
        if action not in ['mark', 'unmark']:
            return None
