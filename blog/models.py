from django.db import models
from django.contrib.auth import get_user_model
from rest_framework.negotiation import BaseContentNegotiation


# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=50, null=False)


class Post(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField()
    picture = models.ImageField(null=True, blank=True)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, default='request.user')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False)
    favorite = models.ManyToManyField(get_user_model(), related_name='was_marked', blank=True)


class IgnoreClientContentNegotiation(BaseContentNegotiation):
    def select_parser(self, request, parsers):
        """
        Select the first parser in the `.parser_classes` list.
        """
        return parsers[0]

    def select_renderer(self, request, renderers, format_suffix):
        """
        Select the first renderer in the `.renderer_classes` list.
        """
        return (renderers[0], renderers[0].media_type)
