from django.shortcuts import render
from rest_framework.viewsets import ViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_202_ACCEPTED, HTTP_400_BAD_REQUEST

from django.shortcuts import get_object_or_404
from .serializers import CategorySerializer, PostSerializer, FavoriteSerializer
from .models import Category, Post


# Create your views here.

class PostView(ViewSet):

    def create(self, request, *args, **kwargs):
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            obj = serializer.save(author=request.user)
            return Response(status=HTTP_202_ACCEPTED, data={'post_id': obj.id})

    def update(self, request, *args, **kwargs):
        try:
            instance = Post.objects.get(id=kwargs.get('pk'), author=request.user)
        except Post.DoesNotExist:
            return Response(status=HTTP_400_BAD_REQUEST,
                            data={'code': 1,
                                  'description': 'Вы пытаетесь редактирвоать пост которого не существует, или пост не ваш'})
        serializer = PostSerializer(instance=instance, data=request.data, partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save(author=request.user)
            return Response(status=HTTP_202_ACCEPTED)

    def retrieve(self, request, *args, **kwargs):
        post = get_object_or_404(Post, id=kwargs.get('pk'))
        responce = PostSerializer(post, context={'request': request})
        return Response(responce.data)

    def destroy(self, request, *args, **kwargs):
        post = get_object_or_404(Post, id=kwargs.get('pk'))
        post.delete()
        return Response(status=HTTP_202_ACCEPTED)


class CategoryView(ViewSet):
    def list(self, request, *args, **kwargs):
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        posts = Post.objects.filter(category=kwargs.get('pk'))
        responce = PostSerializer(posts, many=True)
        return Response(responce.data)


class Favorite(APIView):

    def post(self, request, *args, **kwargs):
        serializator = FavoriteSerializer(data=request.data)
        if serializator.is_valid(raise_exception=True):
            post = Post.objects.get(id=request.data.get('post'))
            if request.data.get('action') == 'mark':
                post.favorite.add(request.user)
            elif request.data.get('action') == 'unmark':
                post.favorite.remove(request.user)
            else:
                return Response(status=HTTP_400_BAD_REQUEST)
            post.save()
            return Response(status=HTTP_202_ACCEPTED)

    def get(self, request, *args, **kwargs):
        posts = Post.objects.filter(favorite=request.user)
        serializator = PostSerializer(posts, many=True)
        return Response(serializator.data)
